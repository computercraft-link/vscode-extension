# ComputerCraft Link

This extension for vs code provides a websocket server that can be used with the [client side script](https://gitlab.com/computercraft-link/cc-tweaked-client) to connect to a [ComputerCraft:Tweaked computer](https://tweaked.cc) in Minecraft.

## Features

- Send a file to Minecraft, these files will then be watched for changes ![](https://user-images.githubusercontent.com/61925890/177252261-9bc661cd-d925-4aa4-bf92-0273c8ed8aae.gif)
- See what files are on the computer in-game
- Delete files on in-game computer ![](https://user-images.githubusercontent.com/61925890/177252344-e59572ef-8f2e-4b1b-b853-ef0212e109dd.gif)

## Requirements

**This extension will not work** unless you have set up the [client script](https://gitlab.com/computercraft-link/cc-tweaked-client) on a computer in Minecraft.

## Release Notes

Check [CHANGELOG.md](CHANGELOG.md)
