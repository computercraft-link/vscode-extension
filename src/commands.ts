import * as vscode from "vscode";
import { TreeComputer, TreeFile, TreeFileRemote, TreeFold } from "./computerTree";
import { Computer, createComputer, removeComputer } from "./computer";
import { Dictionary } from "./types";
import { logger } from "./logging";
import { quickPickComputer } from "./computerQuickPick";
import { createFile, File } from "./File";

const commands = {
  // Create a computer object and generate a UUID for a new computer connection
  connect: (context: vscode.ExtensionContext) => {
    logger.log("[COMMAND] computercraft-link.connect");
    if (vscode.workspace.getWorkspaceFolder.length < 1) {
      vscode.window.showErrorMessage("You must first open a workspace to connect a computer");
      return;
    }

    const uuid = createComputer(context);

    logger.log("Created Computer: " + uuid);

    vscode.env.clipboard.writeText(uuid);
    vscode.window.showInformationMessage("Copied new UUID to clipboard");
  },

  remove: (context: vscode.ExtensionContext, treeItem: TreeComputer) => {
    logger.log("[COMMAND] computercraft-link.remove");
    const uuid = treeItem.computer.uuid;
    removeComputer(context, uuid);
  },

  // Copy the UUID of a TreeComputer object
  copyUUID: (context: vscode.ExtensionContext, treeItem: TreeComputer) => {
    logger.log("[COMMAND] computercraft-link.copyUUID");
    vscode.env.clipboard.writeText(treeItem.computer.uuid);
    vscode.window.showInformationMessage("Copied UUID to clipboard!");
  },

  // Disconnect a computer using a TreeComputer object
  disconnect: (context: vscode.ExtensionContext, treeItem: TreeComputer) => {
    logger.log("[COMMAND] computercraft-link.disconnect");
    treeItem.computer.terminateConnection(context, "The connection has been closed manually");
    logger.log("Closed Websocket manually: " + treeItem.computer.uuid);
  },

  watchFile: async (context: vscode.ExtensionContext, uri: vscode.Uri) => {
    logger.log("[COMMAND] computercraft-link.watchFile");
    const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
    const files: Dictionary<File> = context.workspaceState.get("files", {});

    if (!computers) {
      throw new Error("There are no computers");
    }

    const targetComputer = await quickPickComputer(computers);

    if (!targetComputer) {
      return;
    }

    // File is already being watched
    if (files[uri.toString()]?.targetComputers?.includes(targetComputer.uuid)) {
      logger.log("File is already being watched");
      return;
    }

    const newFile = createFile(context, uri);
    newFile.addTargetComputer(context, targetComputer.uuid);
    newFile.sendToComputers(context);
  },

  stopWatchingFile: (context: vscode.ExtensionContext, treeItem: TreeFile) => {
    logger.log("[COMMAND] computercraft-link.stopWatchingFile");
    treeItem.file.removeComputer(context, treeItem.computer.uuid);
  },

  refreshFiles: (context: vscode.ExtensionContext, treeItem: TreeFold) => {
    logger.log("[COMMAND] computercraft-link.refreshFiles");
    treeItem.computer.requestTree();
  },

  deleteRemoteFile: async (context: vscode.ExtensionContext, treeItem: TreeFileRemote) => {
    logger.log("[COMMAND] computercraft-link.deleteRemoteFile");
    const result = await vscode.window.showQuickPick(["Yes", "No"], {title: `Are you sure you want to delete ${treeItem.path}`, placeHolder: "No"});

    if (result !== "Yes") {
      return;
    }

    treeItem.computer.deleteFile(treeItem.path);
    treeItem.computer.requestTree();
  },
};

export function registerCommands(context: vscode.ExtensionContext) {
  for (const [name, callback] of Object.entries(commands)) {
    vscode.commands.registerCommand(`computercraft-link.${name}`, (args) => {
      callback(context, args);
    });
  }
}
