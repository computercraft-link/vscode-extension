import * as path from 'path';
import * as vscode from "vscode";
import { Computer } from "./computer";
import { File } from "./File";
import { Dictionary } from "./types";

function parseTree(computer: Computer, items: any, path = "") {
  const tree: any = [];

  Object.entries(items).forEach(([name, value]) => {
    const fullPath = `${path}/${name}`;
    if (typeof value === "object") {
      tree.push(new TreeFolderRemote(name, parseTree(computer, value, fullPath), computer, fullPath));
    } else {
      tree.push(new TreeFileRemote(name, computer, fullPath));
    }
  });

  return tree;
}

function buildTreeItems(context: vscode.ExtensionContext): TreeComputer[] {
  const tree: TreeComputer[] = [];

  const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
  const files: Dictionary<File> = context.workspaceState.get("files", {});

  const map: Dictionary<{ computer: Computer; files: File[] }> = {};

  Object.entries(computers).forEach(([uuid, computer]) => {
    map[uuid] = { computer: computer, files: [] };
  });

  Object.entries(files).forEach(([uri, file]) => {
    file.targetComputers.forEach((uuid) => {
      map[uuid].files.push(file);
    });
  });

  Object.entries(map).forEach(([uuid, data]) => {
    const files = data.files.map((file) => new TreeFile(file, data.computer));
    const computer = new TreeComputer(data.computer, files, parseTree(data.computer, data.computer.files));
    tree.push(computer);
  });

  return tree;
}

export class ComputerTreeProvider
  implements vscode.TreeDataProvider<TreeComputer | vscode.TreeItem>
{
  private _onDidChangeTreeData: vscode.EventEmitter<TreeComputer | undefined | null | void> =
    new vscode.EventEmitter<TreeComputer | undefined | null | void>();
  readonly onDidChangeTreeData: vscode.Event<TreeComputer | undefined | null | void> =
    this._onDidChangeTreeData.event;
  data: TreeComputer[];

  constructor(context: vscode.ExtensionContext) {
    this.data = buildTreeItems(context);
  }

  refresh(context: vscode.ExtensionContext): void {
    const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
    vscode.commands.executeCommand(
      "setContext",
      "computercraft-link.computerCount",
      Object.keys(computers).length
    );
    vscode.commands.executeCommand(
      "setContext",
      "computercraft-link.onlineComputerCount",
      Object.values(computers).filter((computer) => computer.isConnected).length
    );

    this.data = buildTreeItems(context);
    this._onDidChangeTreeData.fire();
  }

  getTreeItem(element: TreeComputer): vscode.TreeItem {
    return element;
  }

  getChildren(
    element?: TreeComputer | undefined
  ): vscode.ProviderResult<TreeComputer[] | vscode.TreeItem[]> {
    if (!element) {
      return this.data;
    }
    return element.children;
  }
}

export class TreeFold extends vscode.TreeItem {
  computer: Computer;
  children: vscode.TreeItem[];

  constructor(computer: Computer, name: string, children: vscode.TreeItem[], contextValue?: string) {
    super(name, vscode.TreeItemCollapsibleState.Collapsed);

    this.computer = computer;
    this.children = children;

    this.contextValue = contextValue;
  }
}

export class TreeFile extends vscode.TreeItem {
  file: File;
  computer: Computer;

  constructor(file: File, computer: Computer) {
    super(file.relativePath, vscode.TreeItemCollapsibleState.None);

    this.file = file;
    this.computer = computer;

    this.iconPath = vscode.ThemeIcon.File;
    this.resourceUri = file.uri;
    this.contextValue = "trackedFile";
  }
}

export class TreeFileRemote extends vscode.TreeItem {
  path: string;
  computer: Computer;

  constructor(filename: string, computer: Computer, path: string) {
    super(filename, vscode.TreeItemCollapsibleState.None);
    this.path = path;
    this.computer = computer;

    this.iconPath = vscode.ThemeIcon.File;
    this.contextValue = "remoteFile";
  }
}

export class TreeFolderRemote extends vscode.TreeItem {
  children: vscode.TreeItem[];
  computer: Computer;
  path: String;

  constructor(name: string, children: vscode.TreeItem[], computer: Computer, path: string) {
    super(name, vscode.TreeItemCollapsibleState.Collapsed);
    this.iconPath = vscode.ThemeIcon.Folder;
    this.contextValue = "remoteDir";

    this.children = children;
    this.computer = computer;
    this.path = path;
  }
}
export class TreeComputer extends vscode.TreeItem {
  computer: Computer;
  children: vscode.TreeItem[];

  constructor(computer: Computer, localFiles: TreeFile[], remoteFiles: TreeFileRemote[]) {
    super(
      computer.name,
      computer.isConnected
        ? vscode.TreeItemCollapsibleState.Expanded
        : vscode.TreeItemCollapsibleState.None
    );

    this.description = computer.uuid;
    this.command = { command: "computercraft-link.copyUUID", title: "Copy UUID", arguments: [this], tooltip: "Copy UUID" };

    let lightIcon = "disconnected.svg";
    let darkIcon = "disconnected.svg";

    if (computer.isConnected) {
      lightIcon = "connected.svg";
      darkIcon = "connected.svg";
    }

    this.iconPath = {
      light: path.join(__filename, "..", "..", "assets", lightIcon),
      dark: path.join(__filename, "..", "..", "assets", darkIcon),
    };

    this.computer = computer;

    if (computer.isConnected) {
      if (localFiles.length > 0) {
        this.contextValue = "connectedComputer";
      } else {
        this.contextValue = "connectedComputerNoTracking";
      }
    } else {
      if (localFiles.length > 0) {
        this.contextValue = "disconnectedComputer";
      } else {
        this.contextValue = "disconnectedComputerNoTracking";
      }
    }

    // Add children (local files, remote files)
    this.children = [];

    this.children.push(new TreeFold(computer, "Watching", localFiles));
    this.children.push(new TreeFold(computer, "Files", remoteFiles, "remoteFiles"));
  }
}
