import * as vscode from "vscode";
import { Computer } from "./computer";
import { Dictionary } from "./types";

class ComputerPick implements vscode.QuickPickItem {
  computer: Computer;
  label: string;
  description: string;
  uuid: string;

  constructor(computer: Computer) {
    this.computer = computer;
    this.label = `$(radio-tower) ${computer.name}`;
    this.description = computer.uuid;
    this.uuid = computer.uuid;
  }
}

// Useful for quick picking between the saved computers
// Will prompt the user to pick between the computers
export async function quickPickComputer(computers: Dictionary<Computer>, onlyOnline = true): Promise<Computer|undefined> {
  const asArray = Object.entries(computers);
  const choices = onlyOnline ? asArray.filter(([uuid, computer]) => computer.isConnected) : asArray;

  if (choices.length === 1) {
    return choices[0][1];
  }

  const computerPicks = choices.map(([uuid, computer]) => new ComputerPick(computer));
  const selectedUUID = await vscode.window.showQuickPick(computerPicks);

  return selectedUUID?.computer;
}
