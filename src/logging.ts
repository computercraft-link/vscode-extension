import { TextDecoder, TextEncoder } from "util";
import * as vscode from "vscode";

export class Logger {
  channel: vscode.OutputChannel;

  constructor() {
    this.channel = vscode.window.createOutputChannel("ComputerCraft Link", "markdown");
  }

  async log(text: string) {
    const timestamp = Date.now();
    this.channel.appendLine(`[${timestamp}] | ${text}`);
  }
}

export const logger = new Logger();
