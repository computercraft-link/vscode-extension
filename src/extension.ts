import * as vscode from "vscode";
import * as Websocket from "ws";
import { ComputerTreeProvider } from "./computerTree";
import { Computer } from "./computer";
import { File } from "./File";
import { registerCommands } from "./commands";
import { Dictionary } from "./types";
import { logger } from "./logging";

let wss: Websocket.WebSocketServer;
let watcher: vscode.FileSystemWatcher;
let computerTree: ComputerTreeProvider;

// this method is called when your extension is activated
export function activate(context: vscode.ExtensionContext) {
  const workspace = vscode.workspace.workspaceFolders?.[0];
  let computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
  let files: Dictionary<File> = context.workspaceState.get("files", {});

  logger.log("[INIT] Started!");

  // Register tree view
  computerTree = new ComputerTreeProvider(context);
  vscode.window.registerTreeDataProvider("computercraft-link.computers", computerTree);
  logger.log("[INIT] Registered tree view");
  // Register a vscode command for refreshing the tree
  vscode.commands.registerCommand("computercraft-link.refreshTree", () =>
    computerTree.refresh(context)
  );

  // Restore state and remove tracked files
  for (const [uri, file] of Object.entries(files)) {
    files[uri] = new File(context, vscode.Uri.parse(uri));
  }
  for (const [uuid, computer] of Object.entries(computers)) {
    computers[uuid] = new Computer(computer.name, uuid);
    computers[uuid].terminateConnection(context);
  }
  context.workspaceState.update("computers", computers);
  context.workspaceState.update("files", files);

  vscode.commands.executeCommand(
    "setContext",
    "computercraft-link.computerCount",
    Object.keys(computers).length
  );

  vscode.commands.executeCommand("setContext", "computercraft-link.onlineComputerCount", 0);

  //Register commands
  registerCommands(context);
  logger.log("[INIT] Registered commands");

  // Create websocket server
  wss = new Websocket.WebSocketServer({
    port: 56560,
    handleProtocols: (protocols, request) => "computercraft-link",
    verifyClient: (info, cb) => {
      const token = info?.req?.headers?.authorization;

      if (!token) {
        logger.log("[WEBSOCKET] Unauthorized connection");
        return cb(false, 401, "UUID is expected in Authorization header!");
      }

      if (!computers[token]) {
        logger.log("[WEBSOCKET] Invalid UUID");
        return cb(false, 403, "UUID is not valid!");
      }

      if (computers[token].isConnected) {
        logger.log("[WEBSOCKET] Attempted to connect with UUID of currently connected computer");
        return cb(false, 403, "UUID is already in use");
      }

      return cb(true);
    },
  });

  logger.log("[INIT] Started Websocket");

  wss.addListener("connection", (ws, request) => {
    const token = request?.headers?.authorization;

    if (!workspace) {
      return ws.close(1000, "A workspace is not opened in VS Code");
    }

    if (!token) {
      return ws.close(1000, "UUID is expected in Authorization header!");
    }

    const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
    const computer = computers[token];

    computer.websocket = ws;

    // Assign computer name if given in headers
    const name = request.headers["computer-name"];
    if (name && typeof name === "string") {
      computer.name = name;
    }

    logger.log(`[WEBSOCKET] Computer connected: ${computer.name}(${computer.uuid})`);

    context.workspaceState.update("computers", computers);

    computer.requestTree();

    vscode.commands.executeCommand("computercraft-link.refreshTree");

    ws.addListener("close", (code, reason) => {
      computer.terminateConnection(context);
      logger.log(`[WEBSOCKET] Computer disconnected: ${computer.name}(${computer.uuid})`);
      vscode.commands.executeCommand("computercraft-link.refreshTree");
    });

    ws.addListener("message", (data, isBinary) => {
      const payload = JSON.parse(data.toString("utf-8"));

      if (payload.operation === "dirTree") {
        logger.log(`[WEBSOCKET] Received directory tree from ${computer.name}(${computer.uuid})`);
        computer.files = payload.tree;
      }

      vscode.commands.executeCommand("computercraft-link.refreshTree");
    });
  });

  // Watch files for changes
  // should a watched file change, send it to all computers watching it
  watcher = vscode.workspace.createFileSystemWatcher("**/*.{lua,txt,md}", true, false, true);
  watcher.onDidChange((uri) => {
    const targets = files[uri.toString()]?.targetComputers;

    if (!targets) {
      return;
    }

    logger.log("[WATCHER] Change detected in " + vscode.workspace.asRelativePath(uri));

    targets.forEach((computer) => computers[computer].sendFile(files[uri.toString()]));
  });

  vscode.workspace.onDidRenameFiles((event) => {
    const files: Dictionary<File> = context.workspaceState.get("files", {});
    event.files.forEach((uris) => {
      const newURI = uris.newUri.toString();
      const oldURI = uris.oldUri.toString();
      const oldObject = files[oldURI];

      if (oldObject) {
        // Create new file object from old one
        files[newURI] = new File(context, uris.newUri, oldObject.targetComputers);

        // Send a rename event to watching computers
        files[newURI].targetComputers.forEach((uuid) => {
          computers[uuid].renameFile(uris.oldUri, uris.newUri);
        });

        delete files[oldURI];
      }
    });

    context.workspaceState.update("files", files);
    vscode.commands.executeCommand("computercraft-link.refreshTree");
  });

  logger.log("[INIT] Started watching Lua files");
}

// this method is called when your extension is deactivated
export function deactivate() {
  wss.close();
  watcher.dispose();
}
