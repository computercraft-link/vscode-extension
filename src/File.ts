import * as vscode from "vscode";
import { Computer } from "./Computer";
import { logger } from "./logging";
import { Dictionary } from "./types";

export class File {
  uri: vscode.Uri;
  relativePath: string;
  targetComputers: string[];

  constructor(context: vscode.ExtensionContext, uri: vscode.Uri, targetComputers?: string[]) {
    this.uri = uri;
    this.relativePath = vscode.workspace.asRelativePath(uri);
    this.targetComputers = targetComputers ?? [];
    this.serialize(context);
  }

  private serialize(context: vscode.ExtensionContext) {
    const files: any = context.workspaceState.get("files", {});
    if (this.targetComputers.length === 0) {
      delete files[this.uri.toString()];
    } else {
      files[this.uri.toString()] = this;
    }
    context.workspaceState.update("files", files);
  }

  addTargetComputer(context: vscode.ExtensionContext, uuid: string) {
    this.targetComputers.push(uuid);
    this.serialize(context);
    vscode.commands.executeCommand("computercraft-link.refreshTree");
    const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
    logger.log(`[FILE] ${computers[uuid].name}(${uuid}) started watching file: ${this.relativePath}`);
  }

  removeComputer(context: vscode.ExtensionContext, uuid: string) {
    const index = this.targetComputers.findIndex((id) => id === uuid);
    if (index < 0) {
      return;
    }
    this.targetComputers.splice(index, 1);
    this.serialize(context);
    vscode.commands.executeCommand("computercraft-link.refreshTree");
    const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
    logger.log(`[FILE] ${computers[uuid].name}(${uuid}) stopped watching file: ${this.relativePath}`);
  }

  sendToComputers(context: vscode.ExtensionContext) {
    const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
    const targets = Object.entries(computers).filter(([uuid, computer]) =>
      this.targetComputers.includes(uuid)
    );

    targets.forEach(([uuid, computer]) => {
      computer.sendFile(this);
    });
  }
}

export function createFile(context: vscode.ExtensionContext, uri: vscode.Uri) {
  const files: Dictionary<File> = context.workspaceState.get("files", {});

  if (files[uri.toString()]) {
    return files[uri.toString()];
  }

  return new File(context, uri);
}
