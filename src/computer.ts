import * as vscode from "vscode";
import { WebSocket } from "ws";
import { v4 as uuid4 } from "uuid";
import { logger } from "./logging";
import { Dictionary } from "./types";
import { File } from "./File";

export class Computer {
  name: string;
  uuid: string;
  authenticated: boolean;
  websocket: WebSocket | null;
  files: string[];

  constructor(name: string, uuid?: string) {
    this.name = name;
    this.uuid = uuid || uuid4();

    this.authenticated = false;

    this.websocket = null;
    this.files = [];
  }

  private serialize(context: vscode.ExtensionContext) {
    const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
    computers[this.uuid] = this;
    context.workspaceState.update("computers", computers);
  }

  get isConnected() {
    return this.websocket !== null && this.websocket.readyState === 1;
  }

  terminateConnection(context: vscode.ExtensionContext, reason?: string) {
    if (this.websocket?.readyState === 1 || this.websocket?.readyState === 0) {
      this.websocket?.close(1000, reason);
    }

    const files: Dictionary<File> = context.workspaceState.get("files", {});

    Object.entries(files).forEach(([uri, file]) => {
      file.removeComputer(context, this.uuid);
    });

    logger.log(`Computer disconnected: ${this.name}(${this.uuid})`);

    this.authenticated = false;
    context.workspaceState.update("files", files);
    vscode.commands.executeCommand("computercraft-link.refreshTree");
  }

  sendFile(file: File) {
    vscode.workspace.openTextDocument(file.uri).then((document) => {
      const textContent = document.getText();
      const relativePath = file.relativePath;

      const payload = {
        operation: "write",
        path: relativePath,
        content: textContent,
      };

      this.websocket?.send(JSON.stringify(payload));

      logger.log(`Sent ${relativePath} to ${this.name}(${this.uuid})`);
    });
  }

  deleteFile(path: string) {
    const payload = {
      operation: "delete",
      path: path,
    };

    this.websocket?.send(JSON.stringify(payload));

    logger.log(`Deleting ${path}`);
  }

  renameFile(oldUri: vscode.Uri, newUri: vscode.Uri) {
    const oldPath = vscode.workspace.asRelativePath(oldUri);
    const newPath = vscode.workspace.asRelativePath(newUri);

    logger.log(`Rename in progress: ${oldPath} -> ${newPath}`);

    const payload = {
      operation: "rename",
      oldPath,
      newPath,
    };

    this.websocket?.send(JSON.stringify(payload));
  }

  requestTree() {
    const payload = {
      operation: "dirTree",
    };

    logger.log("Requesting tree");

    this.websocket?.send(JSON.stringify(payload));
  }
}

/**
 * Add a new computer to this workspace's storage
 */
export function createComputer(context: vscode.ExtensionContext) {
  const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});
  const computerCount = Object.keys(computers).length;

  const computer = new Computer(`Computer #${computerCount + 1}`);
  const uuid = computer.uuid;

  computers[uuid] = computer;

  // Update UI computer count
  vscode.commands.executeCommand(
    "setContext",
    "computercraft-link.computerCount",
    computerCount + 1
  );

  context.workspaceState.update("computers", computers);
  vscode.commands.executeCommand("computercraft-link.refreshTree");

  return uuid;
}

/**
 * Removes a computer from this workspace's storage
 */
export function removeComputer(context: vscode.ExtensionContext, uuid: string) {
  const computers: Dictionary<Computer> = context.workspaceState.get("computers", {});

  delete computers[uuid];

  context.workspaceState.update("computers", computers);

  logger.log("Removed Computer: " + uuid);
  vscode.commands.executeCommand("computercraft-link.refreshTree");
}
